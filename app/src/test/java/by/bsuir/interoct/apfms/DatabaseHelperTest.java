package by.bsuir.interoct.apfms;

import by.bsuir.interoct.apfms.data.local.DatabaseHelper;
import by.bsuir.interoct.apfms.data.local.DbOpenHelper;
import by.bsuir.interoct.apfms.util.DefaultConfig;
import by.bsuir.interoct.apfms.util.RxSchedulersOverrideRule;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Unit tests integration with a SQLite Database using Robolectric
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = DefaultConfig.EMULATE_SDK)
public class DatabaseHelperTest {

    @Rule
    public final RxSchedulersOverrideRule mOverrideSchedulersRule = new RxSchedulersOverrideRule();
    private final DatabaseHelper mDatabaseHelper =
            new DatabaseHelper(new DbOpenHelper(RuntimeEnvironment.application));

}