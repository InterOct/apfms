package by.bsuir.interoct.apfms.test.common.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import by.bsuir.interoct.apfms.injection.component.ApplicationComponent;
import by.bsuir.interoct.apfms.test.common.injection.module.ApplicationTestModule;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
