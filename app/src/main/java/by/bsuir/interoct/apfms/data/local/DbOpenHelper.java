package by.bsuir.interoct.apfms.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import by.bsuir.interoct.apfms.data.model.*;
import by.bsuir.interoct.apfms.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;

@Singleton
public class DbOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "app.db";
    public static final int DATABASE_VERSION = 3;

    @Inject
    public DbOpenHelper(@ApplicationContext Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(Category.CREATE_TABLE);
            db.execSQL(Account.CREATE_TABLE);
            db.execSQL(Record.CREATE_TABLE);

            catE(db, "Питание");
            catE(db, "Одежда и косметика");
            catE(db, "Коммунальные расходы");
            catE(db, "Транспорт");
            catE(db, "Образование");
            catE(db, "Развлечения");
            catE(db, "Автомобиль");
            catE(db, "Плата");

            catI(db, "Зарплата");
            catI(db, "Подработка");
            catI(db, "Карманные деньги");
            catI(db, "Возврат займов");
            catI(db, "Прочие доходы");

            acc(db, "Наличные", new BigDecimal(0));
            acc(db, "Кредитная карта", new BigDecimal(0));
            acc(db, "Вклад", new BigDecimal(0));

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private void acc(SQLiteDatabase db, String name, BigDecimal amount) {
        AccountsModel.Insert_account insert_account = new Account.Insert_account(db, Account.FACTORY);
        insert_account.bind(name, amount, null);
        insert_account.program.executeInsert();
    }

    private void catI(SQLiteDatabase db, String name) {
        cat(db, name, CategoryType.INCOME);
    }

    private void catE(SQLiteDatabase db, String name) {
        cat(db, name, CategoryType.EXPENSE);
    }

    private void cat(SQLiteDatabase db, String name, CategoryType type) {
        CategoryModel.Insert_category insert_category = new Category.Insert_category(db, Category.FACTORY);
        insert_category.bind(name, type);
        insert_category.program.executeInsert();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
