package by.bsuir.interoct.apfms.ui.stastic.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.Record;
import by.bsuir.interoct.apfms.data.model.RecordType;
import by.bsuir.interoct.apfms.ui.base.BaseActivity;
import by.bsuir.interoct.apfms.ui.record.RecordDetailsActivity;
import by.bsuir.interoct.apfms.util.ViewUtil;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author aleks
 */
public class StatisticFragment extends Fragment implements StatisticFragmentMvpView, OnChartValueSelectedListener {

    private static final String ARG_TYPE = "by.bsuir.interoct.apfms.ui.stastic.fragment.StatisticFragment.type";

    @Inject
    StatisticFragmentPresenter mPresenter;

    @Inject
    RecordListAdapter recordListAdapter;

    @BindView(R.id.chart) PieChart mChart;
    @BindView(R.id.records_list) ListView recordsList;

    private Unbinder unbinder;
    private RecordType recordType;
    private List<Record.Full> all;

    public StatisticFragment() {
    }

    public static StatisticFragment newInstance(RecordType recordType) {
        StatisticFragment fragment = new StatisticFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, recordType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onResume() {
        super.onResume();
        mPresenter.loadRecords(recordType);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recordType = (RecordType) getArguments().getSerializable(ARG_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).activityComponent().inject(this);
        View view = inflater.inflate(R.layout.fragment_statistic, container, false);
        unbinder = ButterKnife.bind(this, view);
        setPieChart();
        recordsList.setAdapter(recordListAdapter);
        mPresenter.attachView(this);
        mPresenter.loadRecords(recordType);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mPresenter.detachView();
    }

    private void setPieChart() {
        mChart.getDescription().setText("");
        mChart.setOnChartValueSelectedListener(this);
        mChart.setUsePercentValues(true);
        mChart.setExtraOffsets(5, 10, 5, 5);
        mChart.setDragDecelerationFrictionCoef(0.95f);
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);
        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);
        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);
        mChart.setRotationAngle(0);
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);
        mChart.setDrawMarkers(false);
        mChart.getLegend().setEnabled(false);
        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        mChart.setOnChartValueSelectedListener(this);

    }

    private void showPieChart(List<PieEntry> entries) {
        PieDataSet dataSet = new PieDataSet(entries, "Records");
        dataSet.setDrawIcons(false);
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ViewUtil.COLORS);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(12f);
        data.setValueTextColor(Color.WHITE);
        mChart.setEntryLabelTextSize(12f);

        mChart.setData(data);
        mChart.highlightValue(null);

        mChart.invalidate();
    }

    @Override
    public void showError() {

    }

    @Override
    public void showRecords(HashMap<Long, StatisticWrapper> totals, BigDecimal total,
            List<Record.Full> original) {
        all = original;
        onNothingSelected();
        if (!totals.isEmpty()) {
            List<PieEntry> entries = new ArrayList<>();
            for (Map.Entry<Long, StatisticWrapper> entry : totals.entrySet()) {
                entries.add(new PieEntry(entry.getValue().totalAmount.floatValue() / total.floatValue()
                        , entry.getValue().categoryName, entry.getValue().records));
            }
            showPieChart(entries);
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        recordListAdapter.setList((List<Record.Full>) e.getData());
        recordListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNothingSelected() {
        recordListAdapter.setList(all);
        recordListAdapter.notifyDataSetChanged();
    }

    @OnItemClick(R.id.records_list)
    public void onRecordItem(long id, int position) {
        Record.Full item = recordListAdapter.getItem(position);
        Intent intent = new Intent(getContext(), RecordDetailsActivity.class);
        intent.putExtra(RecordDetailsActivity.ARG_RECORD, item);
        startActivity(intent);
    }

}
