package by.bsuir.interoct.apfms.ui.category;

import by.bsuir.interoct.apfms.data.model.Category;
import by.bsuir.interoct.apfms.ui.base.MvpView;
import by.bsuir.interoct.apfms.ui.base.dialog.AddDialogCallback;
import by.bsuir.interoct.apfms.ui.base.dialog.SaveDeleteDialogCallback;

import java.util.List;

public interface CategoryListMvpView extends MvpView, AddDialogCallback, SaveDeleteDialogCallback {
    void showCategories(List<Category> categories);
    void showError();
}
