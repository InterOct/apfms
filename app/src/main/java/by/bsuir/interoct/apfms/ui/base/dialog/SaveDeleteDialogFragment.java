package by.bsuir.interoct.apfms.ui.base.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;
import by.bsuir.interoct.apfms.R;

/**
 * AddDialogFragment
 */
public class SaveDeleteDialogFragment extends DialogFragment {

    private static final String ARG_TITLE = "ARG_TITLE";
    private static final String ARG_DATA = "ARG_DATA";
    private static final String ARG_ID = "ARG_ID";

    private SaveDeleteDialogCallback mCallback;

    private String mTitle;
    private String mData;
    private Long mId;

    public static SaveDeleteDialogFragment newInstance(String title, String data, Long id) {
        Bundle args = new Bundle();
        SaveDeleteDialogFragment fragment = new SaveDeleteDialogFragment();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_DATA, data);
        args.putLong(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_TITLE)) {
            mTitle = getArguments().getString(ARG_TITLE);
            mData = getArguments().getString(ARG_DATA);
            mId = getArguments().getLong(ARG_ID);
        }
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof SaveDeleteDialogCallback) {
            mCallback = (SaveDeleteDialogCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText input = new EditText(getActivity());
        input.setText(mData);
        builder.setView(input)
                .setTitle(mTitle)
                .setPositiveButton(R.string.action_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String name = input.getText().toString();
                        mCallback.onUpdate(mId, name);

                    }
                })
                .setNegativeButton(R.string.action_remove, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDelete(mId);
                    }
                });
        return builder.create();
    }
}
