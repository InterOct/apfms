package by.bsuir.interoct.apfms.data.local;

import android.content.Context;
import android.content.SharedPreferences;
import by.bsuir.interoct.apfms.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PreferencesHelper {

    private static final String PREF_FILE_NAME = "_pref_file";
    private static final String REGISTERED = "by.bsuir.interoct.apfms.data.local.PreferencesHelper.registered";

    private final SharedPreferences mPref;

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public boolean isRegistered(){
        return mPref.getBoolean(REGISTERED, false);
    }

    public void setRegistered(boolean registered) {
        mPref.edit().putBoolean(REGISTERED, registered).apply();
    }

    public void clear() {
        mPref.edit().clear().apply();
    }

}
