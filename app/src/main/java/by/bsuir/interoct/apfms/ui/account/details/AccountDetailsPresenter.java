package by.bsuir.interoct.apfms.ui.account.details;

import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.ui.base.BasePresenter;
import by.bsuir.interoct.apfms.util.RxUtil;
import rx.Observer;
import rx.Subscription;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import javax.inject.Inject;

/**
 * Created by aleks on 4/30/2017.
 */
public class AccountDetailsPresenter extends BasePresenter<AccountDetailsMvpView> {

    private final DataManager dataManager;
    private Subscription mSubscription;

    @Inject
    public AccountDetailsPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void detachView() {
        super.detachView();

        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void insert(Account account) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = dataManager.insertAccount(account).subscribeOn(Schedulers.io()).subscribe();

    }

    public void delete(Account account) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = dataManager.deleteAccount(account)
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Account>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Error insert account");
                    }

                    @Override
                    public void onNext(Account account) {
                    }
                });
    }

    public void update(Account account) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = dataManager.updateAccount(account)
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Account>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Error insert account");
                    }

                    @Override
                    public void onNext(Account account) {
                    }
                });
    }
}
