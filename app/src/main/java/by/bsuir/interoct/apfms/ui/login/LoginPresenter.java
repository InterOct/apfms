package by.bsuir.interoct.apfms.ui.login;

import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.ui.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by aleks on 5/5/2017.
 */
public class LoginPresenter extends BasePresenter<LoginMvpView> {

    private final DataManager mDataManager;

    @Inject
    public LoginPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    public boolean isUserRegistered() {
        return mDataManager.isUserRegistered();
    }

    public void setUserRegistered(boolean registered) {
        mDataManager.setUserRegistered(registered);
    }
}
