package by.bsuir.interoct.apfms.ui.stastic.fragment;

import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.data.model.Record;
import by.bsuir.interoct.apfms.data.model.RecordType;
import by.bsuir.interoct.apfms.ui.base.BasePresenter;
import by.bsuir.interoct.apfms.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by aleks on 5/13/2017.
 */
public class StatisticFragmentPresenter extends BasePresenter<StatisticFragmentMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public StatisticFragmentPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void loadRecords(RecordType type) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.getRecordsWithDesc(type)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Record.Full>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Record.Full> fulls) {
                        BigDecimal total = BigDecimal.valueOf(0);
                        HashMap<Long, StatisticWrapper> totals = new HashMap<>();
                        for (Record.Full full : fulls) {
                            total = total.add(full.records().amount());
                            long key = full.category()._id();
                            StatisticWrapper wrapper = totals.get(key);
                            if (wrapper == null) {
                                wrapper = new StatisticWrapper(full.records().amount(), full.category().name());
                            } else {
                                wrapper.totalAmount = wrapper.totalAmount.add(full.records().amount());
                            }
                            wrapper.records.add(full);
                            totals.put(key, wrapper);
                        }
                        getMvpView().showRecords(totals, total, fulls);
                    }
                });
    }
}
