package by.bsuir.interoct.apfms.data.model;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.squareup.sqldelight.RowMapper;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Created by aleks on 5/1/2017.
 */
@AutoValue
public abstract class Record implements Parcelable, RecordModel, Serializable {

    private static final BigDecimalAdapter AMOUNT_ADAPTER = new BigDecimalAdapter();
    private static final DateAdapter DATE_ADAPTER = new DateAdapter();
    private static final RecordTypeAdapter RECORD_TYPE_ADAPTER = new RecordTypeAdapter();
    public static final Factory<Record> FACTORY = new Factory<>(new RecordModel.Creator<Record>() {
        @Override public Record create(long _id, @NonNull BigDecimal amount, @Nullable Long category_id,
                @Nullable Calendar date,
                @Nullable String note, @Nullable RecordType type, @Nullable Long account_id) {
            return Record.create(_id, amount, category_id, date, note, type, account_id);
        }
    }, AMOUNT_ADAPTER, DATE_ADAPTER, RECORD_TYPE_ADAPTER);
    public static final RowMapper<Full> FULL_ROW_MAPPER = FACTORY.select_by_with_descriptionsMapper(
            new Select_by_with_descriptionsCreator<Record, Category, Account, Full>() {
                @Override public Full create(@NonNull Record records, @NonNull Category category,
                        @NonNull Account accounts) {
                    return new AutoValue_Record_Full(records, category, accounts);
                }
            }, Category.FACTORY, Account.FACTORY);
    public static final RowMapper<Record> MAPPER = FACTORY.select_allMapper();

    public static Builder builder() {
        return new AutoValue_Record.Builder();
    }

    public static Record create(long _id, BigDecimal amount, Long category_id, Calendar date, String note, RecordType type,
            Long account_id) {
        return builder()
                ._id(_id)
                .amount(amount)
                .category_id(category_id)
                .date(date)
                .note(note)
                .type(type)
                .account_id(account_id)
                .build();
    }

    @AutoValue public static abstract class Full
            implements Serializable, Select_by_with_descriptionsModel<Record, Category, Account> {
    }

    @AutoValue.Builder public abstract static class Builder {
        public abstract Builder _id(long _id);

        public abstract Builder amount(BigDecimal amount);

        public abstract Builder category_id(Long category_id);

        public abstract Builder date(Calendar date);

        public abstract Builder note(String note);

        public abstract Builder type(RecordType type);

        public abstract Builder account_id(Long account_id);

        public abstract Record build();
    }
}
