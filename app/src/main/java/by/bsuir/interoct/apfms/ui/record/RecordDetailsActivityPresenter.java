package by.bsuir.interoct.apfms.ui.record;

import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.data.model.Category;
import by.bsuir.interoct.apfms.data.model.CategoryType;
import by.bsuir.interoct.apfms.data.model.Record;
import by.bsuir.interoct.apfms.ui.base.BasePresenter;
import by.bsuir.interoct.apfms.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by aleks on 5/8/2017.
 */
public class RecordDetailsActivityPresenter extends BasePresenter<RecordDetailsMvpView> {

    private DataManager mDataManager;
    private Subscription mSubscription;
    private Subscription mSubscriptionAcc;

    @Inject
    public RecordDetailsActivityPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscriptionAcc.unsubscribe();
        }
    }

    public void loadCategories(CategoryType type) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.getCategoriesByType(type)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Category>>() {
                    @Override public void onCompleted() {
                    }

                    @Override public void onError(Throwable e) {
                        Timber.e(e, "Error loading category expenses");
                        getMvpView().showErrorCategories();

                    }

                    @Override public void onNext(List<Category> categories) {
                        getMvpView().populateCategoriesDropdown(categories);
                    }
                });
    }

    public void loadAccounts() {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscriptionAcc = mDataManager.getAccounts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Account>>() {
                    @Override public void onCompleted() {

                    }

                    @Override public void onError(Throwable e) {
                        Timber.e(e, "Error loading accounts");
                        getMvpView().showErrorAccounts();
                    }

                    @Override public void onNext(List<Account> accounts) {
                        getMvpView().populateAccountsDropdown(accounts);
                    }
                });
    }

    public void insertRecord(Record record) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.insertRecord(record).subscribeOn(Schedulers.io()).subscribe();
    }

    public void updateRecord(Record record) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.updateRecord(record).subscribeOn(Schedulers.io()).subscribe();
    }

    public void deleteRecord(Record record) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.deleteRecord(record).subscribeOn(Schedulers.io()).subscribe();

    }


}
