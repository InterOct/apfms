package by.bsuir.interoct.apfms.ui.base;

import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Branavets_AY
 */
public abstract class BaseListViewAdapter<T, VH extends BaseViewHolder> extends BaseAdapter {

    protected List<T> list;

    public void setList(List<T> list) {
        this.list = list;
    }

    public BaseListViewAdapter() {
        list = new ArrayList<>();
    }

    @Override public int getCount() {
        return list.size();
    }

    @Override public T getItem(int position) {
        return list.get(position);
    }

}
