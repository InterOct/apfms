package by.bsuir.interoct.apfms.data.model;

/**
 * Created by aleks on 5/8/2017.
 */
public enum RecordType {
    EXPENSE(1,CategoryType.EXPENSE), INCOME(2,CategoryType.INCOME), TRANSFER(3, null);

    private long value;
    private CategoryType categoryType;

    RecordType(long value, CategoryType categoryType) {
        this.value = value;
        this.categoryType = categoryType;
    }

    public long getValue() {
        return value;
    }

    public CategoryType getCategoryType() {
        return categoryType;
    }
}
