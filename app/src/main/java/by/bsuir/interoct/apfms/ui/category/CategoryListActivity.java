package by.bsuir.interoct.apfms.ui.category;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.Category;
import by.bsuir.interoct.apfms.data.model.CategoryType;
import by.bsuir.interoct.apfms.ui.base.BaseActivity;
import by.bsuir.interoct.apfms.ui.base.dialog.AddDialogFragment;
import by.bsuir.interoct.apfms.ui.base.dialog.SaveDeleteDialogFragment;
import by.bsuir.interoct.apfms.util.DialogFactory;

import javax.inject.Inject;
import java.util.List;

public class CategoryListActivity extends BaseActivity implements CategoryListMvpView {

    private static final String NEW_DIALOG = "NEW_DIALOG";
    public static final String ARG_CATEGORY_TYPE = "by.bsuir.interoct.apfms.ui.category.CategoryListActivity.ARG_CATEGORY_TYPE";

    @Inject
    CategoryListPresenter mCategoryPresenter;

    @Inject
    CategoryListAdaptor mCategoryListAdaptor;

    @BindView(R.id.category_list)
    ListView mListView;

    private CategoryType categoryType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_category_list);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
        }

        categoryType = (CategoryType) getIntent().getSerializableExtra(ARG_CATEGORY_TYPE);
        setTitle(categoryType.getTitleId());

        mListView.setAdapter(mCategoryListAdaptor);

        mCategoryPresenter.attachView(this);
        mCategoryPresenter.loadCategories(categoryType);
    }

    @OnClick(R.id.action_add)
    public void add() {
        AddDialogFragment.newInstance(getString(R.string.new_category))
                .show(getSupportFragmentManager(), NEW_DIALOG);
    }

    @OnItemClick(R.id.category_list)
    void onListClicked(long id, int position) {
        Category item = (Category) mCategoryListAdaptor.getItem(position);
        SaveDeleteDialogFragment.newInstance(getString(R.string.change_category), item.name(), item._id())
                .show(getSupportFragmentManager(), NEW_DIALOG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mCategoryPresenter.detachView();
    }

    @Override
    public void showCategories(
            List<Category> categories) {
        mCategoryListAdaptor.setCategoryList(categories);
        mCategoryListAdaptor.notifyDataSetChanged();

    }

    @Override
    public void showError() {
        DialogFactory.createGenericErrorDialog(this, getString(R.string.error_loading_categories))
                .show();
    }

    @Override
    public void onSave(String data) {
        mCategoryPresenter.insertCategory(Category.create(0, data, categoryType));
    }

    @Override
    public void onUpdate(Long id, String data) {
        mCategoryPresenter.updateCategory(Category.create(id, data, categoryType));
    }

    @Override
    public void onDelete(Long id) {
        mCategoryPresenter.deleteCategory(Category.create(id, "", categoryType));
    }
}
