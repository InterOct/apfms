package by.bsuir.interoct.apfms.ui.account.details;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.ui.base.BaseActivity;
import by.bsuir.interoct.apfms.util.ViewUtil;

import javax.inject.Inject;

import java.math.BigDecimal;

import static butterknife.ButterKnife.findById;

public class AccountDetailsActivity extends BaseActivity implements AccountDetailsMvpView {

    public static final String ARG_ACCOUNT = "by.bsuir.interoct.apfms.ui.mAccount.details.AccountDetailsActivity.mAccount";

    @Inject
    AccountDetailsPresenter mAccountDetailsPresenter;

    private Account mAccount;

    @BindView(R.id.account_total)
    EditText accountTotal;
    @BindView(R.id.account_name)
    EditText accountName;

    @BindView(R.id.action_remove)
    ImageButton buttonRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_account_details);
        ButterKnife.bind(this);

        Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
        }

        mAccountDetailsPresenter.attachView(this);

        mAccount = getIntent().getParcelableExtra(ARG_ACCOUNT);
        if (mAccount == null) {
            buttonRemove.setVisibility(View.INVISIBLE);
        } else {
            accountTotal.setText(ViewUtil.toString(mAccount.total()));
            accountName.setText(mAccount.payment_name());
        }
    }

    @OnClick(R.id.action_save)
    public void onSave() {
        if (mAccount == null) {
            Account account = Account.create(0,
                    accountName.getText().toString(),
                    new BigDecimal(accountTotal.getText().toString()),
                    null);
            mAccountDetailsPresenter.insert(account);
        } else {
            Account account = buildAccount();
            mAccountDetailsPresenter.update(account);
        }
        finish();
    }

    @OnClick(R.id.action_remove)
    public void onRemove() {
        mAccountDetailsPresenter.delete(buildAccount());
        finish();
    }

    private Account buildAccount() {
        return Account.create(mAccount._id(),
                accountName.getText().toString(),
                new BigDecimal(accountTotal.getText().toString()),
                null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        mAccountDetailsPresenter.detachView();
    }
}
