package by.bsuir.interoct.apfms.ui.base.dialog;

/**
 * Created by aleks on 5/14/2017.
 */
public interface AddDialogCallback {
    void onSave(String data);
}
