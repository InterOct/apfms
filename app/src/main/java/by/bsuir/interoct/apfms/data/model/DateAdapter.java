package by.bsuir.interoct.apfms.data.model;

import android.support.annotation.NonNull;
import com.squareup.sqldelight.ColumnAdapter;

import java.util.Calendar;
import java.util.Date;

public final class DateAdapter implements ColumnAdapter<Calendar, Long> {

  @Override public Long encode(@NonNull Calendar date) {
    return date.getTimeInMillis();
  }

  @NonNull @Override public Calendar decode(Long data) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(data);
    return calendar;
  }
}
