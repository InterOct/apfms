package by.bsuir.interoct.apfms.data.model;

import android.support.annotation.NonNull;
import com.squareup.sqldelight.ColumnAdapter;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Branavets_AY
 */
public class BigDecimalAdapter implements ColumnAdapter<BigDecimal, String> {
    @NonNull
    @Override
    public BigDecimal decode(String databaseValue) {
        return new BigDecimal(databaseValue);
    }

    @Override
    public String encode(@NonNull BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_UP).toString();
    }
}
