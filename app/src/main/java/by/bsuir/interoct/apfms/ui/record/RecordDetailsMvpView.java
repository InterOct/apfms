package by.bsuir.interoct.apfms.ui.record;

import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.data.model.Category;
import by.bsuir.interoct.apfms.ui.base.MvpView;

import java.util.List;

/**
 * Created by aleks on 5/8/2017.
 */
public interface RecordDetailsMvpView extends MvpView {
    void showErrorCategories();

    void populateCategoriesDropdown(List<Category> categoryIncomes);

    void populateAccountsDropdown(List<Account> accounts);

    void showErrorAccounts();
}
