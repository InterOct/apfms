package by.bsuir.interoct.apfms.ui.category;

import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.data.model.Category;
import by.bsuir.interoct.apfms.data.model.CategoryType;
import by.bsuir.interoct.apfms.ui.base.BasePresenter;
import by.bsuir.interoct.apfms.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import javax.inject.Inject;
import java.util.List;

public class CategoryListPresenter extends BasePresenter<CategoryListMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscriptionGet;
    private Subscription mSubscriptionCrud;

    @Inject
    public CategoryListPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(CategoryListMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscriptionGet != null) {
            mSubscriptionGet.unsubscribe();
        }
        if (mSubscriptionCrud != null) {
            mSubscriptionCrud.unsubscribe();
        }
    }

    public void loadCategories(CategoryType categoryType) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscriptionGet);
        mSubscriptionGet = mDataManager.getCategoriesByType(categoryType)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Category>>() {
                    @Override public void onCompleted() {
                    }

                    @Override public void onError(Throwable e) {
                        Timber.e(e, "Error loading category expenses");
                        getMvpView().showError();

                    }

                    @Override public void onNext(List<Category> categoryIncomes) {
                        getMvpView().showCategories(categoryIncomes);
                    }
                });
    }

    public void insertCategory(final Category category) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscriptionCrud);
        mSubscriptionCrud = mDataManager.insertCategory(category)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        new Subscriber<Category>() {
                            @Override
                            public void onCompleted() {
                                loadCategories(category.type());
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Category categoryExpenses) {

                            }
                        });

    }

    public void updateCategory(final Category category) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscriptionCrud);
        mSubscriptionCrud = mDataManager.updateCategory(category)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        new Subscriber<Category>() {
                            @Override
                            public void onCompleted() {
                                loadCategories(category.type());
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Category categoryExpenses) {

                            }
                        });

    }

    public void deleteCategory(final Category category) {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscriptionCrud);
        mSubscriptionCrud = mDataManager.deleteCategory(category)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        new Subscriber<Category>() {
                            @Override
                            public void onCompleted() {
                                loadCategories(category.type());
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Category categoryExpenses) {

                            }
                        });
    }
}
