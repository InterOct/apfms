package by.bsuir.interoct.apfms.ui.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.ui.base.BaseListViewAdapter;
import by.bsuir.interoct.apfms.ui.base.BaseViewHolder;

import javax.inject.Inject;
import java.util.ArrayList;

/**
 * @author Branavets_AY
 */
public class PeriodAdapter extends BaseListViewAdapter<String, PeriodAdapter.ViewHolder> {

    @Inject
    public PeriodAdapter() {
        list = new ArrayList<>();
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.period_content, parent, false);
            viewHolder = new PeriodAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        {
            viewHolder = (PeriodAdapter.ViewHolder) convertView.getTag();
        }
        String record = getItem(position);
        viewHolder.text.setText(record);
        return convertView;
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R.id.text) TextView text;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
