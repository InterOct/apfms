package by.bsuir.interoct.apfms.ui.main;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.util.ViewUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static butterknife.ButterKnife.findById;

/**
 * @author Branavets_AY
 */
public class FromToDateDialog extends DialogFragment {

    private static final String ARG_FROM_DATE = "ARG_FROM_DATE";
    private static final String ARG_TO_DATE = "ARG_TO_DATE";

    private FromToDateDialogCallback callback;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    private Calendar fromDate = ViewUtil.currentDayStart();
    private Calendar toDate = ViewUtil.currentDayEnd();

    public static FromToDateDialog newInstance(Calendar fromDate, Calendar toDate) {
        Bundle args = new Bundle();
        FromToDateDialog fragment = new FromToDateDialog();
        args.putSerializable(ARG_FROM_DATE, fromDate);
        args.putSerializable(ARG_TO_DATE, toDate);
        fragment.setArguments(args);
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fromDate = (Calendar) getArguments().getSerializable(ARG_FROM_DATE);
        toDate = (Calendar) getArguments().getSerializable(ARG_TO_DATE);
    }

    @Override public void onAttach(Context context) {
        if (context instanceof FromToDateDialogCallback) {
            callback = (FromToDateDialogCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FromToDateDialogCallback");
        }
        super.onAttach(context);
    }

    @Override public void onDetach() {
        super.onDetach();
        callback = null;
    }

    @NonNull @Override public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View layout = getActivity().getLayoutInflater()
                .inflate(R.layout.date_dialog_content, null);
        final TextView dateFromTextView = findById(layout, R.id.date_from);
        dateFromTextView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                fromDate = ViewUtil.calendarFrom(year, month, dayOfMonth);
                                dateFromTextView.setText(dateFormat.format(fromDate.getTime()));
                            }
                        },
                        fromDate.get(Calendar.YEAR),
                        fromDate.get(Calendar.MONTH),
                        fromDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        final TextView dateToTextView = findById(layout, R.id.date_to);
        dateToTextView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                toDate = ViewUtil.calendarFrom(year, month, dayOfMonth);
                                dateToTextView.setText(dateFormat.format(toDate.getTime()));
                            }
                        },
                        toDate.get(Calendar.YEAR),
                        toDate.get(Calendar.MONTH),
                        toDate.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        builder.setView(layout)
                .setTitle(getString(R.string.choose_dates))
                .setPositiveButton(R.string.action_add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.dateSelected(ViewUtil.setDayStart(fromDate),
                                ViewUtil.setDayEnd(toDate));
                    }
                });
        return builder.create();
    }
}
