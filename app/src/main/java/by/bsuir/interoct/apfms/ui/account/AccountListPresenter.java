package by.bsuir.interoct.apfms.ui.account;

import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.ui.base.BasePresenter;
import by.bsuir.interoct.apfms.util.RxUtil;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Branavets_AY
 */
public class AccountListPresenter extends BasePresenter<AccountListMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public AccountListPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    @Override public void attachView(AccountListMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override public void detachView() {
        super.detachView();
        if (mSubscription != null)
            mSubscription.unsubscribe();
    }

    public void loadAccounts() {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.getAccounts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Account>>() {
                    @Override public void onCompleted() {

                    }

                    @Override public void onError(Throwable e) {
                        Timber.e(e, "Error loading accounts");
                        getMvpView().showError();
                    }

                    @Override public void onNext(List<Account> accounts) {
                        getMvpView().showAccounts(accounts);
                    }
                });
    }
}
