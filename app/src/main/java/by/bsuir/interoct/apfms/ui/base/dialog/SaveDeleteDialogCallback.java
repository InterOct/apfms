package by.bsuir.interoct.apfms.ui.base.dialog;

/**
 * Created by aleks on 5/14/2017.
 */
public interface SaveDeleteDialogCallback {

    void onUpdate(Long id, String data);

    void onDelete(Long id);
}
