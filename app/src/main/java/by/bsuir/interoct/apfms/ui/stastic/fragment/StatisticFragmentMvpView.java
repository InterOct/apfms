package by.bsuir.interoct.apfms.ui.stastic.fragment;

import by.bsuir.interoct.apfms.data.model.Record;
import by.bsuir.interoct.apfms.ui.base.MvpView;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * Created by aleks on 5/13/2017.
 */
public interface StatisticFragmentMvpView extends MvpView {

    void showError();

    void showRecords(HashMap<Long, StatisticWrapper> totals, BigDecimal total,
            List<Record.Full> fulls);
}
