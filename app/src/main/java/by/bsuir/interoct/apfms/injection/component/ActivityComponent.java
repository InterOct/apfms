package by.bsuir.interoct.apfms.injection.component;

import by.bsuir.interoct.apfms.injection.PerActivity;
import by.bsuir.interoct.apfms.injection.module.ActivityModule;
import by.bsuir.interoct.apfms.ui.account.AccountListActivity;
import by.bsuir.interoct.apfms.ui.account.details.AccountDetailsActivity;
import by.bsuir.interoct.apfms.ui.category.CategoryListActivity;
import by.bsuir.interoct.apfms.ui.login.LoginActivity;
import by.bsuir.interoct.apfms.ui.main.MainActivity;
import by.bsuir.interoct.apfms.ui.record.RecordDetailsActivity;
import by.bsuir.interoct.apfms.ui.stastic.StatisticActivity;
import by.bsuir.interoct.apfms.ui.stastic.fragment.StatisticFragment;
import dagger.Subcomponent;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(CategoryListActivity categoryListActivity);

    void inject(AccountListActivity accountListActivity);

    void inject(AccountDetailsActivity accountDetailsActivity);

    void inject(LoginActivity loginActivity);

    void inject(RecordDetailsActivity recordDetailsActivityActivity);

    void inject(StatisticActivity statisticActivity);

    void inject(StatisticFragment statisticFragment);
}
