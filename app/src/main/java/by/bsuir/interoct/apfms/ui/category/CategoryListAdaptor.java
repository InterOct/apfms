package by.bsuir.interoct.apfms.ui.category;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.data.model.Category;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class CategoryListAdaptor extends BaseAdapter {

    private List<Category> categoryList;

    @Inject
    public CategoryListAdaptor() {
        categoryList = new ArrayList<>();
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    @Override public int getCount() {
        return categoryList.size();
    }

    @Override public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override public long getItemId(int position) {
        return categoryList.get(position)._id();
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        CategoryViewHolder categoryViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_list_content, parent, false);
            categoryViewHolder = new CategoryViewHolder(convertView);
            convertView.setTag(categoryViewHolder);
        } else {
            categoryViewHolder = (CategoryViewHolder) convertView.getTag();
        }
        categoryViewHolder.contentTextView.setText(categoryList.get(position).name());
        return convertView;
    }

    public int getPosition(Long id) {
        for (int i = 0; i < categoryList.size(); i++) {
            Category account = categoryList.get(i);
            if (id == account._id()) {
                return i;
            }
        }
        return 0;
    }

    static class CategoryViewHolder {

        @BindView(R.id.category_item_content)
        TextView contentTextView;

        public CategoryViewHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }
}
