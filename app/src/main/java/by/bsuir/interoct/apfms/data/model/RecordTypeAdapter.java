package by.bsuir.interoct.apfms.data.model;

import android.support.annotation.NonNull;
import com.squareup.sqldelight.ColumnAdapter;

/**
 * Created by aleks on 5/8/2017.
 */
public class RecordTypeAdapter implements ColumnAdapter<RecordType, Long> {

    @NonNull
    @Override
    public RecordType decode(Long databaseValue) {
        if (databaseValue == 1) {
            return RecordType.EXPENSE;
        } else if(databaseValue == 2) {
            return RecordType.INCOME;
        } else {
            return RecordType.TRANSFER;
        }
    }

    @Override
    public Long encode(@NonNull RecordType value) {
        return value.getValue();
    }
}
