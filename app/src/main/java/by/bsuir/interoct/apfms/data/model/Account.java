package by.bsuir.interoct.apfms.data.model;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.squareup.sqldelight.RowMapper;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Branavets_AY
 */
@AutoValue
public abstract class Account implements Parcelable, AccountsModel, Serializable {

    private static final BigDecimalAdapter TOTAL_ADAPTER = new BigDecimalAdapter();

    public static final Factory<Account> FACTORY = new Account.Factory<>(new AccountsModel.Creator<Account>() {
        @Override public Account create(long _id, @NonNull String payment_name, @NonNull BigDecimal total,
                @Nullable String card_number) {
            return Account.create(_id, payment_name, total, card_number);
        }
    }, TOTAL_ADAPTER);

    public static final RowMapper<Account> MAPPER = FACTORY.select_allMapper();
    public static final RowMapper<Account> ROW_MAPPER = FACTORY.select_oneMapper();

    public static Builder builder() {
        return new AutoValue_Account.Builder();
    }

    public static Account create(long _id, String payment_name, BigDecimal total, String card_number) {
        return builder()
                ._id(_id)
                .payment_name(payment_name)
                .total(total)
                .card_number(card_number)
                .build();
    }

    @AutoValue.Builder public abstract static class Builder {
        public abstract Builder _id(long _id);

        public abstract Builder payment_name(String payment_name);

        public abstract Builder total(BigDecimal total);

        public abstract Builder card_number(String card_number);

        public abstract Account build();
    }
}
