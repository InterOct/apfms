package by.bsuir.interoct.apfms.data.model;

import android.support.annotation.NonNull;
import com.squareup.sqldelight.ColumnAdapter;

/**
 * Created by aleks on 5/8/2017.
 */
public class CategoryTypeAdapter implements ColumnAdapter<CategoryType, Long> {

    @NonNull
    @Override
    public CategoryType decode(Long databaseValue) {
        if (databaseValue == 0) {
            return CategoryType.INCOME;
        } else if(databaseValue == 1) {
            return CategoryType.EXPENSE;
        }
        throw new RuntimeException("No such category");
    }

    @Override
    public Long encode(@NonNull CategoryType value) {
        return value.getValue();
    }
}
