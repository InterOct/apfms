package by.bsuir.interoct.apfms.ui.stastic.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.Record;
import by.bsuir.interoct.apfms.data.model.RecordWrapper;
import by.bsuir.interoct.apfms.ui.base.BaseListViewAdapter;
import by.bsuir.interoct.apfms.ui.base.BaseViewHolder;
import by.bsuir.interoct.apfms.util.ViewUtil;

import javax.inject.Inject;
import java.util.ArrayList;

/**
 * @author Branavets_AY
 */
public class RecordListAdapter extends BaseListViewAdapter<Record.Full,RecordListAdapter.ViewHolder> {

    @Inject
    public RecordListAdapter() {
        list = new ArrayList<>();
    }


    @Override public long getItemId(int position) {
        return getItem(position).records()._id();
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.record_row_content, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Record.Full record = getItem(position);
        viewHolder.account.setText(record.accounts().payment_name());
        viewHolder.amount.setText(ViewUtil.toString(record.records().amount()));
        viewHolder.date.setText(ViewUtil.toString(record.records().date()));
        viewHolder.categoryName.setText(record.category().name());
        return convertView;
    }

    static class ViewHolder extends BaseViewHolder {

        @BindView(R.id.account)
        TextView account;

        @BindView(R.id.amount)
        TextView amount;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.category_name)
        TextView categoryName;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
