package by.bsuir.interoct.apfms.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.inputmethod.InputMethodManager;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public final class ViewUtil {

    public static final ArrayList<Integer> COLORS = new ArrayList<Integer>();

    static {
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            COLORS.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            COLORS.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS)
            COLORS.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            COLORS.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            COLORS.add(c);
        COLORS.add(ColorTemplate.getHoloBlue());

    }

    public static float pxToDp(float px) {
        float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
        return px / (densityDpi / 160f);
    }

    public static int dpToPx(int dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
    }

    public static String toString(BigDecimal value) {
        return value.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
    }

    public static Calendar monthPast() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        return calendar;
    }

    public static Calendar currentDayEnd() {
        Calendar today = Calendar.getInstance();
        setDayEnd(today);
        return today;
    }
    public static Calendar currentDayStart() {
        Calendar today = Calendar.getInstance();
        setDayStart(today);
        return today;
    }

    public static Calendar setDayEnd(Calendar day) {
        day.set(Calendar.HOUR, 23);
        day.set(Calendar.MINUTE, 59);
        day.set(Calendar.SECOND, 59);
        return day;
    }

    public static Calendar setDayStart(Calendar day) {
        day.set(Calendar.HOUR, 0);
        day.set(Calendar.MINUTE, 0);
        day.set(Calendar.SECOND, 0);
        return day;
    }

    public static Calendar calendarFrom(int year, int month, int date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, date);
        return calendar;
    }

    public static String toString(Calendar date, boolean time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date.getTime());
    }

    public static String toString(Calendar date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return dateFormat.format(date.getTime());
    }
}
