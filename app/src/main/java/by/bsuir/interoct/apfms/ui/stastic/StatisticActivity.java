package by.bsuir.interoct.apfms.ui.stastic;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.RecordType;
import by.bsuir.interoct.apfms.ui.base.BaseActivity;
import by.bsuir.interoct.apfms.ui.stastic.fragment.StatisticFragment;

import javax.inject.Inject;

import static butterknife.ButterKnife.bind;
import static butterknife.ButterKnife.findById;

public class StatisticActivity extends BaseActivity implements StatisticActivityMvpView {

    @Inject
    StatisticActivityPresenter mPresenter;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_statistic);
        ButterKnife.bind(this);

        setTitle(R.string.main_menu_statistics);

        mPresenter.attachView(this);

        Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mViewPager.setAdapter(
                new StatisticSectionsPagerAdapter(getSupportFragmentManager()));

        tabLayout.setupWithViewPager(mViewPager);

        mPresenter.attachView(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mPresenter.detachView();

    }

    /**
     * Created by aleks on 5/13/2017.
     */
    public class StatisticSectionsPagerAdapter extends FragmentPagerAdapter {

        public StatisticSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return StatisticFragment.newInstance(RecordType.EXPENSE);
                case 1:
                    return StatisticFragment.newInstance(RecordType.INCOME);
                default:
                    throw new IllegalArgumentException("No such statistic fragment");
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.expenses);
                case 1:
                    return getString(R.string.incomes);
                default:
                    throw new IllegalArgumentException("No such statistic fragment");
            }
        }
    }
}
