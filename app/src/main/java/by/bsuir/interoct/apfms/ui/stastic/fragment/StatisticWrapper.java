package by.bsuir.interoct.apfms.ui.stastic.fragment;

import by.bsuir.interoct.apfms.data.model.Record;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleks on 5/14/2017.
 */
public class StatisticWrapper {
    public BigDecimal totalAmount;
    public String categoryName;
    public List<Record.Full> records = new ArrayList<>();

    public StatisticWrapper(BigDecimal totalAmount, String categoryName) {
        this.totalAmount = totalAmount;
        this.categoryName = categoryName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatisticWrapper wrapper = (StatisticWrapper) o;

        if (totalAmount != null ? !totalAmount.equals(wrapper.totalAmount) : wrapper.totalAmount != null) return false;
        if (categoryName != null ? !categoryName.equals(wrapper.categoryName) : wrapper.categoryName != null)
            return false;
        return records != null ? records.equals(wrapper.records) : wrapper.records == null;
    }

    @Override
    public int hashCode() {
        int result = totalAmount != null ? totalAmount.hashCode() : 0;
        result = 31 * result + (categoryName != null ? categoryName.hashCode() : 0);
        result = 31 * result + (records != null ? records.hashCode() : 0);
        return result;
    }
}
