package by.bsuir.interoct.apfms.data.model;

import android.os.Parcelable;
import com.google.auto.value.AutoValue;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * @author Branavets_AY
 */
@AutoValue
public abstract class RecordWrapper implements Parcelable {

    public abstract long _id();

    public abstract BigDecimal amount();

    public abstract String category();

    public abstract Calendar date();

    public abstract String note();

    public abstract RecordType type();

    public abstract String accountName();

    public static RecordWrapper create(long _id, BigDecimal amount, String category, Calendar date, String note,
            RecordType type,
            String accountName) {
        return builder()
                ._id(_id)
                .amount(amount)
                .category(category)
                .date(date)
                .note(note)
                .type(type)
                .accountName(accountName)
                .build();
    }

    public static Builder builder() {
        return new AutoValue_RecordWrapper.Builder();
    }

    @AutoValue.Builder public abstract static class Builder {
        public abstract Builder _id(long _id);

        public abstract Builder amount(BigDecimal amount);

        public abstract Builder category(String category);

        public abstract Builder date(Calendar date);

        public abstract Builder note(String note);

        public abstract Builder type(RecordType type);

        public abstract Builder accountName(String accountName);

        public abstract RecordWrapper build();
    }
}
