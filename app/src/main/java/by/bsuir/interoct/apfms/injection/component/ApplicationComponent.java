package by.bsuir.interoct.apfms.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.data.local.DatabaseHelper;
import by.bsuir.interoct.apfms.data.local.PreferencesHelper;
import by.bsuir.interoct.apfms.data.remote.RibotsService;
import by.bsuir.interoct.apfms.injection.ApplicationContext;
import by.bsuir.interoct.apfms.injection.module.ApplicationModule;
import by.bsuir.interoct.apfms.util.RxEventBus;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    @ApplicationContext Context context();
    Application application();
    RibotsService ribotsService();
    PreferencesHelper preferencesHelper();
    DatabaseHelper databaseHelper();
    DataManager dataManager();
    RxEventBus eventBus();

}
