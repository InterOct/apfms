package by.bsuir.interoct.apfms.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.CategoryType;
import by.bsuir.interoct.apfms.data.model.RecordType;
import by.bsuir.interoct.apfms.ui.account.AccountListActivity;
import by.bsuir.interoct.apfms.ui.base.BaseActivity;
import by.bsuir.interoct.apfms.ui.category.CategoryListActivity;
import by.bsuir.interoct.apfms.ui.login.LoginActivity;
import by.bsuir.interoct.apfms.ui.record.RecordDetailsActivity;
import by.bsuir.interoct.apfms.ui.settings.SettingsActivity;
import by.bsuir.interoct.apfms.ui.stastic.StatisticActivity;
import by.bsuir.interoct.apfms.util.ViewUtil;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;

import static butterknife.ButterKnife.findById;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, FromToDateDialogCallback, MainActivityMvpView {

    @Inject
    MainActivityPresenter mPresenter;
    @Inject
    PeriodAdapter periodAdapter;

    @BindView(R.id.spend_value)
    TextView spendValue;
    @BindView(R.id.earn_value)
    TextView earnValue;
    @BindView(R.id.period_spinner) Spinner spinner;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    private Calendar fromDate = ViewUtil.currentDayStart();
    private Calendar toDate = ViewUtil.currentDayEnd();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setTitle(ViewUtil.toString(Calendar.getInstance()));

        Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findById(this, R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        periodAdapter.setList(Arrays.asList(
                getString(R.string.day),
                getString(R.string.week),
                getString(R.string.month),
                getString(R.string.year),
                getString(R.string.custom_period)

        ));
        spinner.setAdapter(periodAdapter);

        mPresenter.attachView(this);

        if (!mPresenter.isUserRegistered()) {
            startActivity(new Intent(this, LoginActivity.class));
        }

        mPresenter.loadRecords(fromDate, toDate);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mPresenter.loadRecords(fromDate, toDate);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.loadRecords(fromDate, toDate);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.nav_categories_expenses:
                intent = new Intent(this, CategoryListActivity.class);
                intent.putExtra(CategoryListActivity.ARG_CATEGORY_TYPE, CategoryType.EXPENSE);
                startActivity(intent);
                break;
            case R.id.nav_categories_incomes:
                intent = new Intent(this, CategoryListActivity.class);
                intent.putExtra(CategoryListActivity.ARG_CATEGORY_TYPE, CategoryType.INCOME);
                startActivity(intent);
                break;
            case R.id.nav_accounts:
                startActivity(new Intent(this, AccountListActivity.class));
                break;
            case R.id.nav_statistics:
                startActivity(new Intent(this, StatisticActivity.class));
                break;
            case R.id.nav_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick(R.id.action_add)
    void onPlusClick() {
        Intent intent = new Intent(this, RecordDetailsActivity.class);
        intent.putExtra(RecordDetailsActivity.ARG_RECORD_TYPE, RecordType.INCOME);
        startActivity(intent);
    }

    @OnClick(R.id.action_substract)
    void onSubstractClick() {
        Intent intent = new Intent(this, RecordDetailsActivity.class);
        intent.putExtra(RecordDetailsActivity.ARG_RECORD_TYPE, RecordType.EXPENSE);
        startActivity(intent);
    }

    @Override
    public void showTotal(BigDecimal incomeTotal, BigDecimal expensesTotal) {
        int progress;
        BigDecimal total = incomeTotal.add(expensesTotal);
        if (!BigDecimal.ZERO.equals(total)) {
            progress = (int) (incomeTotal.floatValue() / (total).floatValue() * 100);
        } else {
            progress = 50;
        }
        progressBar.setProgress(progress);
        spendValue.setText(ViewUtil.toString(expensesTotal));
        earnValue.setText(ViewUtil.toString(incomeTotal));
    }

    @Override
    public void showRecordsLoadError() {

    }

    @OnItemSelected(R.id.period_spinner)
    void onPeriodClicked(int position) {
        Calendar from = ViewUtil.currentDayEnd();
        if (getString(R.string.day).equals(periodAdapter.getItem(position))) {
            ViewUtil.setDayStart(from);
            setTitle(ViewUtil.toString(Calendar.getInstance(), true));
        } else if (getString(R.string.week).equals(periodAdapter.getItem(position))) {
            from.add(Calendar.DAY_OF_MONTH, -7);
            setTitle(ViewUtil.toString(from, true) + " - " + ViewUtil.toString(Calendar.getInstance(), true));
        } else if (getString(R.string.month).equals(periodAdapter.getItem(position))) {
            from.add(Calendar.MONTH, -1);
            setTitle(ViewUtil.toString(from, true) + " - " + ViewUtil.toString(Calendar.getInstance(), true));
        } else if (getString(R.string.year).equals(periodAdapter.getItem(position))) {
            from.add(Calendar.YEAR, -1);
            setTitle(ViewUtil.toString(from, true) + " - " + ViewUtil.toString(Calendar.getInstance(), true));
        } else if (getString(R.string.custom_period).equals(periodAdapter.getItem(position))) {
            FromToDateDialog.newInstance(ViewUtil.currentDayStart(), ViewUtil.currentDayEnd())
                    .show(getSupportFragmentManager(), "TO_FROM_DATE_DIALOG");
            from = fromDate;
            setTitle(ViewUtil.toString(fromDate, true) + " - " + ViewUtil.toString(toDate, true));
        }
        mPresenter.loadRecords(from, toDate);
    }

    @Override public void dateSelected(Calendar from, Calendar to) {
        fromDate = from;
        toDate = to;
        mPresenter.loadRecords(from, to);
    }
}
