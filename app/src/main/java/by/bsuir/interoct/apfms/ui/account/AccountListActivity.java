package by.bsuir.interoct.apfms.ui.account;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.ui.account.details.AccountDetailsActivity;
import by.bsuir.interoct.apfms.ui.base.BaseActivity;
import by.bsuir.interoct.apfms.util.DialogFactory;

import javax.inject.Inject;
import java.util.List;

import static butterknife.ButterKnife.findById;

public class AccountListActivity extends BaseActivity implements AccountListMvpView {
    @Inject
    AccountListPresenter mPresenter;

    @Inject
    AccountListAdaptor mAdaptor;

    @BindView(R.id.account_list)
    ListView accountListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_account_list);
        ButterKnife.bind(this);

        Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowCustomEnabled(true);
        }

        accountListView.setAdapter(mAdaptor);

        mPresenter.attachView(this);
        mPresenter.loadAccounts();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mPresenter.loadAccounts();
    }

    @OnClick(R.id.action_add)
    public void add() {
        startActivity(new Intent(this, AccountDetailsActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public void showAccounts(List<Account> accountList) {
        mAdaptor.setList(accountList);
        mAdaptor.notifyDataSetChanged();
    }

    @Override
    public void showError() {
        DialogFactory.createGenericErrorDialog(this, getString(R.string.error_loading_accounts))
                .show();
    }

    @OnItemClick(R.id.account_list)
    public void onItemClicked(long id, int position) {
        Account item = mAdaptor.getItem(position);
        Intent intent = new Intent(this, AccountDetailsActivity.class);
        intent.putExtra(AccountDetailsActivity.ARG_ACCOUNT, (Parcelable) item);
        startActivity(intent);
    }

}
