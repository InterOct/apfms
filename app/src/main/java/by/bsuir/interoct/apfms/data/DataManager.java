package by.bsuir.interoct.apfms.data;

import by.bsuir.interoct.apfms.data.local.DatabaseHelper;
import by.bsuir.interoct.apfms.data.local.PreferencesHelper;
import by.bsuir.interoct.apfms.data.model.*;
import by.bsuir.interoct.apfms.data.remote.RibotsService;
import rx.Observable;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

@Singleton
public class DataManager {

    private final RibotsService mRibotsService;
    private final DatabaseHelper mDatabaseHelper;
    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public DataManager(RibotsService ribotsService, PreferencesHelper preferencesHelper,
            DatabaseHelper databaseHelper) {
        mRibotsService = ribotsService;
        mPreferencesHelper = preferencesHelper;
        mDatabaseHelper = databaseHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public Observable<List<Category>> getCategories() {
        return mDatabaseHelper.getCategory();
    }

    public Observable<List<Category>> getCategoriesByType(CategoryType type) {
        return mDatabaseHelper.getCategoryByType(type);
    }

    public Observable<Category> insertCategory(
            Category category) {
        return mDatabaseHelper.insertCategory(category);
    }

    public Observable<Category> updateCategory(
            Category category) {
        return mDatabaseHelper.updateCategory(category);
    }

    public Observable<Category> deleteCategory(
            Category category) {
        return mDatabaseHelper.deleteCategory(category);
    }

    public Observable<List<Account>> getAccounts() {
        return mDatabaseHelper.getAccounts();
    }

    public Observable<Account> insertAccount(
            Account account) {
        return mDatabaseHelper.insertAccount(account);
    }

    public Observable<Account> updateAccount(
            Account account) {
        return mDatabaseHelper.updateAccount(account);
    }

    public Observable<BigDecimal> updateAccountTotal(long id, BigDecimal total) {
        return mDatabaseHelper.updateAccountTotal(id, total);
    }

    public Observable<Account> deleteAccount(
            Account account) {
        return mDatabaseHelper.deleteAccount(account);
    }

    public boolean isUserRegistered() {
        return mPreferencesHelper.isRegistered();
    }

    public void setUserRegistered(boolean registered) {
        mPreferencesHelper.setRegistered(registered);
    }

    public void clear() {
        mPreferencesHelper.clear();
    }

    public Observable<List<Record>> getRecords() {
        return mDatabaseHelper.getRecords();
    }

    public Observable<List<Category>> getCategory() {
        return mDatabaseHelper.getCategory();
    }

    public Observable<List<Record.Full>> getRecordsWithDesc(RecordType recordType) {
        return mDatabaseHelper.getRecordsWithDesc(recordType);
    }

    public Observable<Record> insertRecord(Record record) {
        return mDatabaseHelper.insertRecord(record);
    }

    public Observable<Record> updateRecord(Record record) {
        return mDatabaseHelper.updateRecord(record);
    }

    public Observable<Record> deleteRecord(Record record) {
        return mDatabaseHelper.deleteRecord(record);
    }

    public Observable<List<Record>> getRecordsInRange(Calendar fromDate, Calendar toDate) {
        return mDatabaseHelper.getRecordsInRange(fromDate, toDate);
    }

}
