package by.bsuir.interoct.apfms.data.model;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.squareup.sqldelight.RowMapper;

import java.io.Serializable;

/**
 * @author Branavets_AY
 */
@AutoValue
public abstract class Category implements Parcelable, CategoryModel, Serializable {

    private static final CategoryTypeAdapter TYPE_ADAPTER = new CategoryTypeAdapter();

    public static final Factory<Category> FACTORY = new Factory<>(new CategoryModel.Creator<Category>() {
        @Override public Category create(long _id, @NonNull String name, @NonNull CategoryType type) {
            return Category.create(_id, name, type);
        }
    }, TYPE_ADAPTER);

    public static final RowMapper<Category> MAPPER = FACTORY.select_allMapper();

    public static Builder builder() {
        return new AutoValue_Category.Builder();
    }

    public static TypeAdapter<Category> typeAdapter(Gson gson) {
        return new AutoValue_Category.GsonTypeAdapter(gson);
    }

    public static Category create(long _id, String name, CategoryType type) {
        return builder()
                ._id(_id)
                .name(name)
                .type(type)
                .build();
    }

    @AutoValue.Builder public abstract static class Builder {
        public abstract Builder _id(long _id);

        public abstract Builder name(String name);

        public abstract Builder type(CategoryType type);

        public abstract Category build();
    }
}
