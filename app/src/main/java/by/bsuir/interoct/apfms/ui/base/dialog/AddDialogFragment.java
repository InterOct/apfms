package by.bsuir.interoct.apfms.ui.base.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;
import by.bsuir.interoct.apfms.R;

/**
 * AddDialogFragment
 */
public class AddDialogFragment extends DialogFragment {

    private static final String ARG_TITLE = "ARG_TITLE";
    private AddDialogCallback mCallback;
    private String mTitle;

    public static AddDialogFragment newInstance(String title) {
        Bundle args = new Bundle();
        AddDialogFragment fragment = new AddDialogFragment();
        args.putString(ARG_TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_TITLE)) {
            mTitle = getArguments().getString(ARG_TITLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof AddDialogCallback) {
            mCallback = (AddDialogCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText input = new EditText(getActivity());
        builder.setView(input)
                .setTitle(mTitle)
                .setPositiveButton(R.string.action_add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String name = input.getText().toString();
                        mCallback.onSave(name);
                    }
                });
        return builder.create();
    }
}
