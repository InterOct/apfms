package by.bsuir.interoct.apfms.ui.main;

import by.bsuir.interoct.apfms.ui.base.MvpView;

import java.math.BigDecimal;

/**
 * Created by aleks on 5/5/2017.
 */
public interface MainActivityMvpView extends MvpView {
    void showTotal(BigDecimal incomeTotal, BigDecimal expensesTotal);
    void showRecordsLoadError();
}
