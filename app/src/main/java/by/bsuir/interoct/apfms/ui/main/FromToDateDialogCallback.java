package by.bsuir.interoct.apfms.ui.main;

import java.util.Calendar;

/**
 * @author Branavets_AY
 */
public interface FromToDateDialogCallback {
    void dateSelected(Calendar from, Calendar to);
}
