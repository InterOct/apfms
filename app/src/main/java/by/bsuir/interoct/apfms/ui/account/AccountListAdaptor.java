package by.bsuir.interoct.apfms.ui.account;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.ui.base.BaseListViewAdapter;
import by.bsuir.interoct.apfms.ui.base.BaseViewHolder;

import javax.inject.Inject;

/**
 * @author Branavets_AY
 */
public class AccountListAdaptor extends BaseListViewAdapter<Account, AccountListAdaptor.AccountVH> {

    @Inject
    public AccountListAdaptor() {
    }

    @Override
    public long getItemId(int position) {
        return list.get(position)._id();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AccountVH viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.account_list_content, parent, false);
            viewHolder = new AccountVH(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AccountVH) convertView.getTag();
        }

        Account item = getItem(position);
        viewHolder.accountName.setText(item.payment_name());
        viewHolder.accountTotal.setText(item.total().setScale(2).toString());
        return convertView;
    }

    public int getPosition(Long id) {
        for (int i = 0; i < list.size(); i++) {
            Account account = list.get(i);
            if (id == account._id()) {
                return i;
            }
        }
        return 0;
    }

    static class AccountVH extends BaseViewHolder {
        @BindView(R.id.account_name)
        TextView accountName;

        @BindView(R.id.account_total)
        TextView accountTotal;

        public AccountVH(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
