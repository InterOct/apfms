package by.bsuir.interoct.apfms.ui.stastic;

import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.ui.base.BasePresenter;
import rx.Subscription;

import javax.inject.Inject;

/**
 * @author Branavets_AY
 */
public class StatisticActivityPresenter extends BasePresenter<StatisticActivityMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public StatisticActivityPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    @Override public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

}
