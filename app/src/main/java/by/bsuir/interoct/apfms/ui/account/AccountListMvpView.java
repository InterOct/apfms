package by.bsuir.interoct.apfms.ui.account;

import android.widget.ListView;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.ui.base.MvpView;

import java.util.List;

/**
 * @author Branavets_AY
 */
public interface AccountListMvpView extends MvpView {
    void showAccounts(List<Account> accountList);
    void showError();
}
