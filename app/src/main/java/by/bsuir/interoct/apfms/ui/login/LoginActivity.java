package by.bsuir.interoct.apfms.ui.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.ui.base.BaseActivity;
import timber.log.Timber;

import javax.inject.Inject;

public class LoginActivity extends BaseActivity implements LoginMvpView {

    @Inject
    LoginPresenter mPresenter;
    @BindView(R.id.login)
    EditText login;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.button_login)
    Button buttonLogin;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mPresenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mPresenter.detachView();
    }

    @OnClick(R.id.button_login)
    public void onLoginClicked() {
        mPresenter.setUserRegistered(true);
        login.setVisibility(View.GONE);
        password.setVisibility(View.GONE);
        image.setVisibility(View.GONE);
        buttonLogin.setVisibility(View.GONE);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            Timber.e(e.getMessage());
        }
        finish();
    }
}
