package by.bsuir.interoct.apfms.data.local;

import android.database.Cursor;
import android.support.annotation.NonNull;
import by.bsuir.interoct.apfms.data.model.*;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;
import com.squareup.sqldelight.SqlDelightStatement;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

@Singleton
public class DatabaseHelper {

    private final BriteDatabase mDb;

    @Inject
    public DatabaseHelper(DbOpenHelper dbOpenHelper) {
        SqlBrite.Builder briteBuilder = new SqlBrite.Builder();
        mDb = briteBuilder.build().wrapDatabaseHelper(dbOpenHelper, Schedulers.immediate());
    }

    public BriteDatabase getBriteDb() {
        return mDb;
    }

    public Observable<List<Category>> getCategory() {
        return mDb.createQuery(Category.TABLE_NAME, Category.SELECT_ALL).mapToList(
                new Func1<Cursor, Category>() {
                    @Override
                    public Category call(Cursor cursor) {
                        return Category.MAPPER.map(cursor);
                    }
                });
    }

    public Observable<List<Category>> getCategoryByType(CategoryType type) {
        String statement = Category.FACTORY.select_by_type(type).statement;
        return mDb.createQuery(Category.TABLE_NAME, statement).mapToList(
                new Func1<Cursor, Category>() {
                    @Override
                    public Category call(Cursor cursor) {
                        return Category.MAPPER.map(cursor);
                    }
                });
    }

    public Observable<Category> insertCategory(final Category category) {
        return Observable.create(new Observable.OnSubscribe<Category>() {
            @Override
            public void call(Subscriber<? super Category> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                Category.Insert_category insert_category = new Category.Insert_category(
                        mDb.getWritableDatabase(), Category.FACTORY);
                insert_category.bind(category.name(), category.type());
                long result = insert_category.program.executeInsert();
                if (result >= 0)
                    subscriber.onNext(category);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<Category> updateCategory(final Category category) {
        return Observable.create(new Observable.OnSubscribe<Category>() {
            @Override
            public void call(Subscriber<? super Category> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                Category.Update_category update_category = new Category.Update_category(
                        mDb.getWritableDatabase());
                update_category.bind(category.name(), category._id());
                long result = update_category.program.executeUpdateDelete();
                if (result >= 0)
                    subscriber.onNext(category);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<Category> deleteCategory(final Category category) {
        return Observable.create(new Observable.OnSubscribe<Category>() {
            @Override
            public void call(Subscriber<? super Category> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                Category.Delete_category delete_category = new Category.Delete_category(
                        mDb.getWritableDatabase());
                delete_category.bind(category._id());
                long result = delete_category.program.executeUpdateDelete();
                if (result >= 0)
                    subscriber.onNext(category);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<List<Account>> getAccounts() {
        return mDb.createQuery(Account.TABLE_NAME, Account.SELECT_ALL).mapToList(
                new Func1<Cursor, Account>() {
                    @Override
                    public Account call(Cursor cursor) {
                        return Account.MAPPER.map(cursor);
                    }
                });
    }

    public Observable<Account> insertAccount(final Account account) {
        return Observable.create(new Observable.OnSubscribe<Account>() {
            @Override
            public void call(Subscriber<? super Account> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                Account.Insert_account insert_account = new Account.Insert_account(
                        mDb.getWritableDatabase(), Account.FACTORY);
                insert_account.bind(account.payment_name(), account.total(), account.card_number());
                long result = insert_account.program.executeInsert();
                if (result >= 0)
                    subscriber.onNext(account);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<Account> updateAccount(final Account account) {
        return Observable.create(new Observable.OnSubscribe<Account>() {
            @Override
            public void call(Subscriber<? super Account> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                Account.Update_account_info update_account = new Account.Update_account_info(
                        mDb.getWritableDatabase(), Account.FACTORY);
                update_account.bind(account.payment_name(), account.total(), account.card_number(), account._id());
                long result = update_account.program.executeUpdateDelete();
                if (result >= 0)
                    subscriber.onNext(account);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<BigDecimal> updateAccountTotal(final long id, final BigDecimal total) {
        return Observable.create(new Observable.OnSubscribe<BigDecimal>() {
            @Override
            public void call(Subscriber<? super BigDecimal> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                long result = DatabaseHelper.this.executeUpdateAccTotal(id, total);
                if (result >= 0)
                    subscriber.onNext(total);
                subscriber.onCompleted();
            }
        });
    }

    private long executeUpdateAccTotal(long id, BigDecimal total) {
        Account.Update_account_total update_account = new Account.Update_account_total(
                mDb.getWritableDatabase(), Account.FACTORY);
        update_account.bind(total, id);
        return (long) update_account.program.executeUpdateDelete();
    }

    public Observable<Account> deleteAccount(final Account account) {
        return Observable.create(new Observable.OnSubscribe<Account>() {
            @Override
            public void call(Subscriber<? super Account> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                Account.Delete_account delete_account = new Account.Delete_account(
                        mDb.getWritableDatabase());
                delete_account.bind(account._id());
                long result = delete_account.program.executeUpdateDelete();
                if (result >= 0)
                    subscriber.onNext(account);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<List<Record>> getRecords() {
        return mDb.createQuery(Record.TABLE_NAME, Record.SELECT_ALL).mapToList(
                new Func1<Cursor, Record>() {
                    @Override
                    public Record call(Cursor cursor) {
                        return Record.MAPPER.map(cursor);
                    }
                });
    }

    public Observable<List<Record.Full>> getRecordsWithDesc(RecordType recordType) {
        String statement = Record.FACTORY.select_by_with_descriptions(recordType).statement;
        return mDb.createQuery(Record.TABLE_NAME, statement).mapToList(
                new Func1<Cursor, Record.Full>() {
                    @Override
                    public Record.Full call(Cursor cursor) {
                        return Record.FULL_ROW_MAPPER.map(cursor);
                    }
                });
    }

    public Observable<List<Record>> getRecordsInRange(Calendar fromDate, Calendar toDate) {
        SqlDelightStatement statement = Record.FACTORY.select_in_date_range(fromDate, toDate);
        return mDb.createQuery(Record.TABLE_NAME, statement.statement).mapToList(
                new Func1<Cursor, Record>() {
                    @Override
                    public Record call(Cursor cursor) {
                        return Record.MAPPER.map(cursor);
                    }
                });
    }

    public Observable<Record> insertRecord(@NonNull final Record record) {
        return Observable.create(new Observable.OnSubscribe<Record>() {
            @Override
            public void call(final Subscriber<? super Record> subscriber) {
                BriteDatabase.Transaction transaction = mDb.newTransaction();
                try {
                    if (subscriber.isUnsubscribed())
                        return;
                    Record.Insert_record insert_record = new Record.Insert_record(
                            mDb.getWritableDatabase(), Record.FACTORY);
                    insert_record.bind(record.amount(), record.category_id(), record.date()
                            , record.note(), record.type(), record.account_id());
                    long result = insert_record.program.executeInsert();
                    updateAccountTotal(record);
                    if (result >= 0)
                        subscriber.onNext(record);
                    subscriber.onCompleted();
                    transaction.markSuccessful();
                } finally {
                    transaction.end();
                }
            }
        });
    }

    private void updateAccountTotal(@NonNull final Record record) {
        Long id = record.account_id();
        if (id != null) {
            String statement = Account.FACTORY.select_one(id).statement;
            Cursor cursor = mDb.query(statement);
            if (cursor.moveToFirst()) {
                Account account = Account.MAPPER.map(cursor);
                if (RecordType.INCOME.equals(record.type())) {
                    executeUpdateAccTotal(account._id(), account.total().add(record.amount()));
                } else if (RecordType.EXPENSE.equals(record.type())) {
                    executeUpdateAccTotal(account._id(), account.total().subtract(record.amount()));
                }
            }
        }
    }

    public Observable<Record> updateRecord(final Record record) {
        return Observable.create(new Observable.OnSubscribe<Record>() {
            @Override
            public void call(Subscriber<? super Record> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                Record.Update_record update_record = new Record.Update_record(
                        mDb.getWritableDatabase(), Record.FACTORY);
                update_record.bind(record.amount(), record.category_id(), record.date()
                        , record.note(), record.type(), record.account_id(), record._id());
                long result = update_record.program.executeUpdateDelete();
                if (result >= 0)
                    subscriber.onNext(record);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<Record> deleteRecord(final Record record) {
        return Observable.create(new Observable.OnSubscribe<Record>() {
            @Override
            public void call(Subscriber<? super Record> subscriber) {
                if (subscriber.isUnsubscribed())
                    return;
                Record.Delete_record delete_record = new Record.Delete_record(
                        mDb.getWritableDatabase());
                delete_record.bind(record._id());
                long result = delete_record.program.executeUpdateDelete();
                if (result >= 0)
                    subscriber.onNext(record);
                subscriber.onCompleted();
            }
        });
    }
}
