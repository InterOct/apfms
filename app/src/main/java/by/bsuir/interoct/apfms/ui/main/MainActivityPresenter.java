package by.bsuir.interoct.apfms.ui.main;

import by.bsuir.interoct.apfms.data.DataManager;
import by.bsuir.interoct.apfms.data.model.Record;
import by.bsuir.interoct.apfms.data.model.RecordType;
import by.bsuir.interoct.apfms.ui.base.BasePresenter;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

/**
 * Created by aleks on 5/5/2017.
 */
public class MainActivityPresenter extends BasePresenter<MainActivityMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public MainActivityPresenter(DataManager mDataManager) {
        this.mDataManager = mDataManager;
    }

    public boolean isUserRegistered() {
        return mDataManager.isUserRegistered();
    }

    public void loadRecords(Calendar from, Calendar to) {
        mSubscription = mDataManager.getRecordsInRange(from, to)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<Record>>() {
                    @Override public void onCompleted() {
                    }

                    @Override public void onError(Throwable e) {
                        Timber.e(e, "Error loading records");
                        getMvpView().showRecordsLoadError();
                    }

                    @Override public void onNext(List<Record> records) {
                        BigDecimal expensesTotal = BigDecimal.valueOf(0);
                        BigDecimal incomeTotal = BigDecimal.valueOf(0);
                        for (Record record : records) {
                            if (RecordType.EXPENSE.equals(record.type())) {
                                expensesTotal = expensesTotal.add(record.amount());
                            } else if (RecordType.INCOME.equals(record.type())) {
                                incomeTotal = incomeTotal.add(record.amount());
                            }
                        }
                        getMvpView().showTotal(incomeTotal,expensesTotal);
                    }
                });
    }

}
