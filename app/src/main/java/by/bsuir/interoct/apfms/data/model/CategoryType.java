package by.bsuir.interoct.apfms.data.model;

import by.bsuir.interoct.apfms.R;

/**
 * @author Branavets_AY
 */
public enum CategoryType {
    INCOME(0, R.string.title_category_incomes), EXPENSE(1,R.string.title_category_expenses);

    private long type;
    private int titleId;

    CategoryType(long type, int titleId) {
        this.type = type;
        this.titleId = titleId;
    }

    public long getValue() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }
}
