package by.bsuir.interoct.apfms.ui.record;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import by.bsuir.interoct.apfms.R;
import by.bsuir.interoct.apfms.data.model.Account;
import by.bsuir.interoct.apfms.data.model.Category;
import by.bsuir.interoct.apfms.data.model.Record;
import by.bsuir.interoct.apfms.data.model.RecordType;
import by.bsuir.interoct.apfms.ui.account.AccountListAdaptor;
import by.bsuir.interoct.apfms.ui.base.BaseActivity;
import by.bsuir.interoct.apfms.ui.category.CategoryListAdaptor;
import by.bsuir.interoct.apfms.util.DialogFactory;
import by.bsuir.interoct.apfms.util.ViewUtil;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import static butterknife.ButterKnife.findById;

public class RecordDetailsActivity extends BaseActivity implements RecordDetailsMvpView {

    public static final String ARG_RECORD_TYPE = "by.bsuir.interoct.apfms.ui.record.RecordDetailsActivity.recordType";
    public static final String ARG_RECORD = "by.bsuir.interoct.apfms.ui.record.RecordDetailsActivity.record";

    @Inject
    AccountListAdaptor accountListAdaptor;

    @Inject
    CategoryListAdaptor categoryListAdaptor;

    @Inject
    RecordDetailsActivityPresenter mPresenter;

    @BindView(R.id.category_spinner)
    Spinner categorySpinner;
    @BindView(R.id.account_spinner)
    Spinner accountSpinner;
    @BindView(R.id.amount_editText)
    EditText amountEditText;
    @BindView(R.id.action_remove)
    Button removeBtn;
    @BindView(R.id.date)
    EditText date;

    private Long categoryId;
    private Long accountId;
    private RecordType recordType;
    private Record.Full record;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_record);
        ButterKnife.bind(this);
        setTitle(R.string.title_operation);
        Toolbar toolbar = findById(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        recordType = (RecordType) getIntent().getSerializableExtra(ARG_RECORD_TYPE);
        record = (Record.Full) getIntent().getSerializableExtra(ARG_RECORD);
        date.setText(ViewUtil.toString(Calendar.getInstance()));
        if (record != null) {
            removeBtn.setVisibility(View.VISIBLE);
            recordType = record.records().type();
            amountEditText.setText(ViewUtil.toString(record.records().amount()));
            date.setText(ViewUtil.toString(record.records().date()));
        }

        categorySpinner.setAdapter(categoryListAdaptor);
        accountSpinner.setAdapter(accountListAdaptor);

        mPresenter.attachView(this);
        mPresenter.loadAccounts();
        mPresenter.loadCategories(recordType.getCategoryType());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mPresenter.detachView();
    }

    @Override
    public void populateCategoriesDropdown(List<Category> categories) {
        categoryListAdaptor.setCategoryList(categories);
        if (record != null) {
            categorySpinner.setSelection(categoryListAdaptor.getPosition(record.records().category_id()));
        }
        categoryListAdaptor.notifyDataSetChanged();
    }

    @Override
    public void populateAccountsDropdown(List<Account> accounts) {
        accountListAdaptor.setList(accounts);
        if (record != null) {
            accountSpinner.setSelection(accountListAdaptor.getPosition(record.records().account_id()));
        }
        accountListAdaptor.notifyDataSetChanged();
    }

    @Override
    public void showErrorAccounts() {
        DialogFactory.createGenericErrorDialog(this, getString(R.string.error_loading_accounts))
                .show();
    }

    @Override
    public void showErrorCategories() {
        DialogFactory.createGenericErrorDialog(this, getString(R.string.error_loading_categories))
                .show();
    }

    @OnItemSelected(R.id.category_spinner)
    void onCategorySpinnerClicked(int position) {
        categoryId = categoryListAdaptor.getItemId(position);
    }

    @OnItemSelected(R.id.account_spinner)
    void onAccountSpinnerClicked(int position) {
        accountId = accountListAdaptor.getItemId(position);
    }

    @OnClick(R.id.action_save)
    void onActionSave() {
        amountEditText.setError(null);
        String amount = amountEditText.getText().toString();
        if (amount != null && !amount.trim().isEmpty()) {
            if (record == null) {
                mPresenter.insertRecord(Record.create(0, new BigDecimal(amount),
                        categoryId, Calendar.getInstance(), null, recordType, accountId));
            } else {
                mPresenter.updateRecord(
                        Record.create(record.records()._id(), new BigDecimal(amount), categoryId, Calendar.getInstance(), null,
                                record.records().type(), accountId));
            }
            finish();
        } else {
            amountEditText.setError(getString(R.string.error_capture_amount));
        }
    }

    @OnClick(R.id.action_remove)
    void onRemove() {
        mPresenter.deleteRecord(record.records());
        finish();
    }
}
